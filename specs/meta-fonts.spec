%global d_conf                  %{_sysconfdir}/fonts
%global d_fonts                 %{_datadir}/fonts/fonts.custom.d

Name:                           meta-fonts
Version:                        1.0.0
Release:                        2%{?dist}
Summary:                        META-package for install and configure fonts
License:                        GPLv3

Source10:                       local.conf

Requires:                       freetype
Requires:                       google-roboto-fonts google-roboto-condensed-fonts
Requires:                       google-noto-sans-fonts google-noto-serif-fonts
Requires:                       google-noto-emoji-fonts google-noto-emoji-color-fonts
Requires:                       google-noto-music-fonts google-noto-cjk-fonts
Requires:                       mozilla-fira-sans-fonts mozilla-fira-mono-fonts

%description
META-package for install and configure fonts.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -dp -m 0755 %{buildroot}%{d_fonts}

%{__install} -Dp -m 0644 %{SOURCE10} \
    %{buildroot}%{d_conf}/local.conf


%files
%dir %{d_fonts}
%config %{d_conf}/local.conf


%changelog
* Sun Jul 28 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-2
- UPD: SPEC-file.

* Sat Jul 27 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-1
- Initial build.
